<?php

namespace ProPhp\PackagistApi;

use ProPhp\Curl\Curl;
use ProPhp\Curl\CurlParams;

class ApiClient
{
    private string $username;
    private string $apiToken;
    private string $host;

    public function __construct(string $username, string $apiToken, string $host = 'packagist.org')
    {
        $this->username = $username;
        $this->apiToken = $apiToken;
        $this->host = $host;
    }

    public function createPackage(string $repoUrl, bool $echo = true)
    {
        $response = Curl::json(
            "https://{$this->host}/api/create-package",
            (new CurlParams())
                ->method('POST')
                ->urlQueryData(['username' => $this->username, 'apiToken' => $this->apiToken])
                ->postRequestData(['repository' => ['url' => $repoUrl]])
        );

        if($echo){
            echo "Packagist package created: " . PHP_EOL . json_encode($response, JSON_PRETTY_PRINT);
        }
    }

    /* Should work in private packagist only, not allowed in public packagist (only from UI)
    public function deletePackage(string $packageTitle)
    {
        $defaultUsername = (new ReposMap())->getDefaultUsername();

        $response = Curl::json(
            "https://packagist.org/api/packages/$packageTitle",
            (new CurlParams())
                ->method('DELETE')
                ->urlQueryData(['username' => $defaultUsername, 'apiToken' => $this->packagistToken])
        );

        showln("Delete packagist package '$packageTitle'", $response);
    }
    */
}