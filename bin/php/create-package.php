<?php

require_once __DIR__ . "/bootstrap.php";

use ProPhp\PackagistApi\ApiClient;

$ApiClient = new ApiClient($username, $apiToken);

if (empty($argv[2])) {
    throw new Exception("Repository URL is missing (\$argv[2])");
}

$repoUrl = $argv[2];

$ApiClient->createPackage($repoUrl);