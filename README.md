# ◦ Docker

`NB!` Excluded from the `vendor` package

```
docker/run
```

Use `docker/stop-all` if required

# ◦ Usage

### ▸ Terminal

```
bin/create-package <username> <repository url>
```

```sh
bin/create-package prophp https://gitlab.com/prophp/packagist-api.git
```

### ▸ Code

```php
use ProPhp\PackagistApi\ApiClient;

$username = 'prophp';
$apiToken = 'a-valid-token-here';
$repoUrl = 'https://gitlab.com/prophp/packagist-api.git';

(new ApiClient($username, $apiToken))->createPackage($repoUrl);
```